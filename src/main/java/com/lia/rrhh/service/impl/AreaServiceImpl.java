package com.lia.rrhh.service.impl;

import com.lia.rrhh.domain.Area;
import com.lia.rrhh.repository.AreaRepository;
import com.lia.rrhh.service.AreaService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.lia.rrhh.domain.Area}.
 */
@Service
@Transactional
public class AreaServiceImpl implements AreaService {

    private final Logger log = LoggerFactory.getLogger(AreaServiceImpl.class);

    private final AreaRepository areaRepository;

    public AreaServiceImpl(AreaRepository areaRepository) {
        this.areaRepository = areaRepository;
    }

    @Override
    public Area save(Area area) {
        log.debug("Request to save Area : {}", area);
        return areaRepository.save(area);
    }

    @Override
    public Area update(Area area) {
        log.debug("Request to update Area : {}", area);
        return areaRepository.save(area);
    }

    @Override
    public Optional<Area> partialUpdate(Area area) {
        log.debug("Request to partially update Area : {}", area);

        return areaRepository
            .findById(area.getId())
            .map(existingArea -> {
                if (area.getNameArea() != null) {
                    existingArea.setNameArea(area.getNameArea());
                }
                if (area.getDescription() != null) {
                    existingArea.setDescription(area.getDescription());
                }

                return existingArea;
            })
            .map(areaRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Area> findAll() {
        log.debug("Request to get all Areas");
        return areaRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Area> findOne(Long id) {
        log.debug("Request to get Area : {}", id);
        return areaRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Area : {}", id);
        areaRepository.deleteById(id);
    }
}
