package com.lia.rrhh.service.impl;

import com.lia.rrhh.domain.Proyect;
import com.lia.rrhh.repository.ProyectRepository;
import com.lia.rrhh.service.ProyectService;
import java.util.List;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link com.lia.rrhh.domain.Proyect}.
 */
@Service
@Transactional
public class ProyectServiceImpl implements ProyectService {

    private final Logger log = LoggerFactory.getLogger(ProyectServiceImpl.class);

    private final ProyectRepository proyectRepository;

    public ProyectServiceImpl(ProyectRepository proyectRepository) {
        this.proyectRepository = proyectRepository;
    }

    @Override
    public Proyect save(Proyect proyect) {
        log.debug("Request to save Proyect : {}", proyect);
        return proyectRepository.save(proyect);
    }

    @Override
    public Proyect update(Proyect proyect) {
        log.debug("Request to update Proyect : {}", proyect);
        return proyectRepository.save(proyect);
    }

    @Override
    public Optional<Proyect> partialUpdate(Proyect proyect) {
        log.debug("Request to partially update Proyect : {}", proyect);

        return proyectRepository
            .findById(proyect.getId())
            .map(existingProyect -> {
                if (proyect.getNameProyect() != null) {
                    existingProyect.setNameProyect(proyect.getNameProyect());
                }
                if (proyect.getDescriptionProyect() != null) {
                    existingProyect.setDescriptionProyect(proyect.getDescriptionProyect());
                }
                if (proyect.getStartDate() != null) {
                    existingProyect.setStartDate(proyect.getStartDate());
                }
                if (proyect.getEndDate() != null) {
                    existingProyect.setEndDate(proyect.getEndDate());
                }

                return existingProyect;
            })
            .map(proyectRepository::save);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Proyect> findAll() {
        log.debug("Request to get all Proyects");
        return proyectRepository.findAll();
    }

    public Page<Proyect> findAllWithEagerRelationships(Pageable pageable) {
        return proyectRepository.findAllWithEagerRelationships(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<Proyect> findOne(Long id) {
        log.debug("Request to get Proyect : {}", id);
        return proyectRepository.findOneWithEagerRelationships(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Proyect : {}", id);
        proyectRepository.deleteById(id);
    }
}
