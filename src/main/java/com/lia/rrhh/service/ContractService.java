package com.lia.rrhh.service;

import com.lia.rrhh.domain.Contract;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.lia.rrhh.domain.Contract}.
 */
public interface ContractService {
    /**
     * Save a contract.
     *
     * @param contract the entity to save.
     * @return the persisted entity.
     */
    Contract save(Contract contract);

    /**
     * Updates a contract.
     *
     * @param contract the entity to update.
     * @return the persisted entity.
     */
    Contract update(Contract contract);

    /**
     * Partially updates a contract.
     *
     * @param contract the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Contract> partialUpdate(Contract contract);

    /**
     * Get all the contracts.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Contract> findAll(Pageable pageable);

    /**
     * Get the "id" contract.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Contract> findOne(Long id);

    /**
     * Delete the "id" contract.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
