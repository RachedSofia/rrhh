package com.lia.rrhh.service;

import com.lia.rrhh.domain.Area;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.lia.rrhh.domain.Area}.
 */
public interface AreaService {
    /**
     * Save a area.
     *
     * @param area the entity to save.
     * @return the persisted entity.
     */
    Area save(Area area);

    /**
     * Updates a area.
     *
     * @param area the entity to update.
     * @return the persisted entity.
     */
    Area update(Area area);

    /**
     * Partially updates a area.
     *
     * @param area the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Area> partialUpdate(Area area);

    /**
     * Get all the areas.
     *
     * @return the list of entities.
     */
    List<Area> findAll();

    /**
     * Get the "id" area.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Area> findOne(Long id);

    /**
     * Delete the "id" area.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
