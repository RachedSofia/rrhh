package com.lia.rrhh.service;

import com.lia.rrhh.domain.Proyect;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link com.lia.rrhh.domain.Proyect}.
 */
public interface ProyectService {
    /**
     * Save a proyect.
     *
     * @param proyect the entity to save.
     * @return the persisted entity.
     */
    Proyect save(Proyect proyect);

    /**
     * Updates a proyect.
     *
     * @param proyect the entity to update.
     * @return the persisted entity.
     */
    Proyect update(Proyect proyect);

    /**
     * Partially updates a proyect.
     *
     * @param proyect the entity to update partially.
     * @return the persisted entity.
     */
    Optional<Proyect> partialUpdate(Proyect proyect);

    /**
     * Get all the proyects.
     *
     * @return the list of entities.
     */
    List<Proyect> findAll();

    /**
     * Get all the proyects with eager load of many-to-many relationships.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<Proyect> findAllWithEagerRelationships(Pageable pageable);

    /**
     * Get the "id" proyect.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<Proyect> findOne(Long id);

    /**
     * Delete the "id" proyect.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
