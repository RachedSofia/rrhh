package com.lia.rrhh.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Proyect.
 */
@Entity
@Table(name = "proyect")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Proyect implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "name_proyect")
    private String nameProyect;

    @Column(name = "description_proyect")
    private String descriptionProyect;

    @Column(name = "start_date")
    private Instant startDate;

    @Column(name = "end_date")
    private Instant endDate;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "rel_proyect__employees",
        joinColumns = @JoinColumn(name = "proyect_id"),
        inverseJoinColumns = @JoinColumn(name = "employees_id")
    )
    @JsonIgnoreProperties(value = { "contracts", "manager", "role", "area", "proyects" }, allowSetters = true)
    private Set<Employee> employees = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "employees", "proyects", "department" }, allowSetters = true)
    private Area areaDestination;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Proyect id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameProyect() {
        return this.nameProyect;
    }

    public Proyect nameProyect(String nameProyect) {
        this.setNameProyect(nameProyect);
        return this;
    }

    public void setNameProyect(String nameProyect) {
        this.nameProyect = nameProyect;
    }

    public String getDescriptionProyect() {
        return this.descriptionProyect;
    }

    public Proyect descriptionProyect(String descriptionProyect) {
        this.setDescriptionProyect(descriptionProyect);
        return this;
    }

    public void setDescriptionProyect(String descriptionProyect) {
        this.descriptionProyect = descriptionProyect;
    }

    public Instant getStartDate() {
        return this.startDate;
    }

    public Proyect startDate(Instant startDate) {
        this.setStartDate(startDate);
        return this;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return this.endDate;
    }

    public Proyect endDate(Instant endDate) {
        this.setEndDate(endDate);
        return this;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Set<Employee> getEmployees() {
        return this.employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }

    public Proyect employees(Set<Employee> employees) {
        this.setEmployees(employees);
        return this;
    }

    public Proyect addEmployees(Employee employee) {
        this.employees.add(employee);
        return this;
    }

    public Proyect removeEmployees(Employee employee) {
        this.employees.remove(employee);
        return this;
    }

    public Area getAreaDestination() {
        return this.areaDestination;
    }

    public void setAreaDestination(Area area) {
        this.areaDestination = area;
    }

    public Proyect areaDestination(Area area) {
        this.setAreaDestination(area);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Proyect)) {
            return false;
        }
        return getId() != null && getId().equals(((Proyect) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Proyect{" +
            "id=" + getId() +
            ", nameProyect='" + getNameProyect() + "'" +
            ", descriptionProyect='" + getDescriptionProyect() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            "}";
    }
}
