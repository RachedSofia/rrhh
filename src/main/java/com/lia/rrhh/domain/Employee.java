package com.lia.rrhh.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Employee.
 */
@Entity
@Table(name = "employee")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Employee implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(name = "cuil")
    private Long cuil;

    @Column(name = "dni")
    private Long dni;

    @Column(name = "birth_date")
    private Instant birthDate;

    @Column(name = "hire_date")
    private Instant hireDate;

    @Column(name = "salary")
    private Long salary;

    @Column(name = "absence")
    private Integer absence;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
    @JsonIgnoreProperties(value = { "employee" }, allowSetters = true)
    private Set<Contract> contracts = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "contracts", "manager", "role", "area", "proyects" }, allowSetters = true)
    private Employee manager;

    @ManyToOne(fetch = FetchType.LAZY)
    private Role role;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "employees", "proyects", "department" }, allowSetters = true)
    private Area area;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "employees")
    @JsonIgnoreProperties(value = { "employees", "areaDestination" }, allowSetters = true)
    private Set<Proyect> proyects = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Employee id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public Employee firstName(String firstName) {
        this.setFirstName(firstName);
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public Employee lastName(String lastName) {
        this.setLastName(lastName);
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return this.email;
    }

    public Employee email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return this.phoneNumber;
    }

    public Employee phoneNumber(String phoneNumber) {
        this.setPhoneNumber(phoneNumber);
        return this;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Long getCuil() {
        return this.cuil;
    }

    public Employee cuil(Long cuil) {
        this.setCuil(cuil);
        return this;
    }

    public void setCuil(Long cuil) {
        this.cuil = cuil;
    }

    public Long getDni() {
        return this.dni;
    }

    public Employee dni(Long dni) {
        this.setDni(dni);
        return this;
    }

    public void setDni(Long dni) {
        this.dni = dni;
    }

    public Instant getBirthDate() {
        return this.birthDate;
    }

    public Employee birthDate(Instant birthDate) {
        this.setBirthDate(birthDate);
        return this;
    }

    public void setBirthDate(Instant birthDate) {
        this.birthDate = birthDate;
    }

    public Instant getHireDate() {
        return this.hireDate;
    }

    public Employee hireDate(Instant hireDate) {
        this.setHireDate(hireDate);
        return this;
    }

    public void setHireDate(Instant hireDate) {
        this.hireDate = hireDate;
    }

    public Long getSalary() {
        return this.salary;
    }

    public Employee salary(Long salary) {
        this.setSalary(salary);
        return this;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    public Integer getAbsence() {
        return this.absence;
    }

    public Employee absence(Integer absence) {
        this.setAbsence(absence);
        return this;
    }

    public void setAbsence(Integer absence) {
        this.absence = absence;
    }

    public Set<Contract> getContracts() {
        return this.contracts;
    }

    public void setContracts(Set<Contract> contracts) {
        if (this.contracts != null) {
            this.contracts.forEach(i -> i.setEmployee(null));
        }
        if (contracts != null) {
            contracts.forEach(i -> i.setEmployee(this));
        }
        this.contracts = contracts;
    }

    public Employee contracts(Set<Contract> contracts) {
        this.setContracts(contracts);
        return this;
    }

    public Employee addContract(Contract contract) {
        this.contracts.add(contract);
        contract.setEmployee(this);
        return this;
    }

    public Employee removeContract(Contract contract) {
        this.contracts.remove(contract);
        contract.setEmployee(null);
        return this;
    }

    public Employee getManager() {
        return this.manager;
    }

    public void setManager(Employee employee) {
        this.manager = employee;
    }

    public Employee manager(Employee employee) {
        this.setManager(employee);
        return this;
    }

    public Role getRole() {
        return this.role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Employee role(Role role) {
        this.setRole(role);
        return this;
    }

    public Area getArea() {
        return this.area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public Employee area(Area area) {
        this.setArea(area);
        return this;
    }

    public Set<Proyect> getProyects() {
        return this.proyects;
    }

    public void setProyects(Set<Proyect> proyects) {
        if (this.proyects != null) {
            this.proyects.forEach(i -> i.removeEmployees(this));
        }
        if (proyects != null) {
            proyects.forEach(i -> i.addEmployees(this));
        }
        this.proyects = proyects;
    }

    public Employee proyects(Set<Proyect> proyects) {
        this.setProyects(proyects);
        return this;
    }

    public Employee addProyects(Proyect proyect) {
        this.proyects.add(proyect);
        proyect.getEmployees().add(this);
        return this;
    }

    public Employee removeProyects(Proyect proyect) {
        this.proyects.remove(proyect);
        proyect.getEmployees().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Employee)) {
            return false;
        }
        return getId() != null && getId().equals(((Employee) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Employee{" +
            "id=" + getId() +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", email='" + getEmail() + "'" +
            ", phoneNumber='" + getPhoneNumber() + "'" +
            ", cuil=" + getCuil() +
            ", dni=" + getDni() +
            ", birthDate='" + getBirthDate() + "'" +
            ", hireDate='" + getHireDate() + "'" +
            ", salary=" + getSalary() +
            ", absence=" + getAbsence() +
            "}";
    }
}
