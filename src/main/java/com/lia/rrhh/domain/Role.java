package com.lia.rrhh.domain;

import jakarta.persistence.*;
import java.io.Serializable;

/**
 * A Role.
 */
@Entity
@Table(name = "role")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "name_role")
    private String nameRole;

    @Column(name = "responsability")
    private String responsability;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Role id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameRole() {
        return this.nameRole;
    }

    public Role nameRole(String nameRole) {
        this.setNameRole(nameRole);
        return this;
    }

    public void setNameRole(String nameRole) {
        this.nameRole = nameRole;
    }

    public String getResponsability() {
        return this.responsability;
    }

    public Role responsability(String responsability) {
        this.setResponsability(responsability);
        return this;
    }

    public void setResponsability(String responsability) {
        this.responsability = responsability;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Role)) {
            return false;
        }
        return getId() != null && getId().equals(((Role) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Role{" +
            "id=" + getId() +
            ", nameRole='" + getNameRole() + "'" +
            ", responsability='" + getResponsability() + "'" +
            "}";
    }
}
