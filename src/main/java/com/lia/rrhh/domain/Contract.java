package com.lia.rrhh.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;

/**
 * A Contract.
 */
@Entity
@Table(name = "contract")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Contract implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "monthly_gross")
    private Long monthlyGross;

    @Column(name = "holidays")
    private Integer holidays;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "contracts", "manager", "role", "area", "proyects" }, allowSetters = true)
    private Employee employee;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Contract id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getMonthlyGross() {
        return this.monthlyGross;
    }

    public Contract monthlyGross(Long monthlyGross) {
        this.setMonthlyGross(monthlyGross);
        return this;
    }

    public void setMonthlyGross(Long monthlyGross) {
        this.monthlyGross = monthlyGross;
    }

    public Integer getHolidays() {
        return this.holidays;
    }

    public Contract holidays(Integer holidays) {
        this.setHolidays(holidays);
        return this;
    }

    public void setHolidays(Integer holidays) {
        this.holidays = holidays;
    }

    public Employee getEmployee() {
        return this.employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Contract employee(Employee employee) {
        this.setEmployee(employee);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Contract)) {
            return false;
        }
        return getId() != null && getId().equals(((Contract) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Contract{" +
            "id=" + getId() +
            ", monthlyGross=" + getMonthlyGross() +
            ", holidays=" + getHolidays() +
            "}";
    }
}
