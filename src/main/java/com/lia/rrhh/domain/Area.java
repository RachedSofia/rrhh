package com.lia.rrhh.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Area.
 */
@Entity
@Table(name = "area")
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Area implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @Column(name = "name_area")
    private String nameArea;

    @Column(name = "description")
    private String description;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "area")
    @JsonIgnoreProperties(value = { "contracts", "manager", "role", "area", "proyects" }, allowSetters = true)
    private Set<Employee> employees = new HashSet<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "areaDestination")
    @JsonIgnoreProperties(value = { "employees", "areaDestination" }, allowSetters = true)
    private Set<Proyect> proyects = new HashSet<>();

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = { "areas" }, allowSetters = true)
    private Department department;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Area id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameArea() {
        return this.nameArea;
    }

    public Area nameArea(String nameArea) {
        this.setNameArea(nameArea);
        return this;
    }

    public void setNameArea(String nameArea) {
        this.nameArea = nameArea;
    }

    public String getDescription() {
        return this.description;
    }

    public Area description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Employee> getEmployees() {
        return this.employees;
    }

    public void setEmployees(Set<Employee> employees) {
        if (this.employees != null) {
            this.employees.forEach(i -> i.setArea(null));
        }
        if (employees != null) {
            employees.forEach(i -> i.setArea(this));
        }
        this.employees = employees;
    }

    public Area employees(Set<Employee> employees) {
        this.setEmployees(employees);
        return this;
    }

    public Area addEmployee(Employee employee) {
        this.employees.add(employee);
        employee.setArea(this);
        return this;
    }

    public Area removeEmployee(Employee employee) {
        this.employees.remove(employee);
        employee.setArea(null);
        return this;
    }

    public Set<Proyect> getProyects() {
        return this.proyects;
    }

    public void setProyects(Set<Proyect> proyects) {
        if (this.proyects != null) {
            this.proyects.forEach(i -> i.setAreaDestination(null));
        }
        if (proyects != null) {
            proyects.forEach(i -> i.setAreaDestination(this));
        }
        this.proyects = proyects;
    }

    public Area proyects(Set<Proyect> proyects) {
        this.setProyects(proyects);
        return this;
    }

    public Area addProyect(Proyect proyect) {
        this.proyects.add(proyect);
        proyect.setAreaDestination(this);
        return this;
    }

    public Area removeProyect(Proyect proyect) {
        this.proyects.remove(proyect);
        proyect.setAreaDestination(null);
        return this;
    }

    public Department getDepartment() {
        return this.department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Area department(Department department) {
        this.setDepartment(department);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Area)) {
            return false;
        }
        return getId() != null && getId().equals(((Area) o).getId());
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Area{" +
            "id=" + getId() +
            ", nameArea='" + getNameArea() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
