package com.lia.rrhh.repository;

import com.lia.rrhh.domain.Proyect;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;

public interface ProyectRepositoryWithBagRelationships {
    Optional<Proyect> fetchBagRelationships(Optional<Proyect> proyect);

    List<Proyect> fetchBagRelationships(List<Proyect> proyects);

    Page<Proyect> fetchBagRelationships(Page<Proyect> proyects);
}
