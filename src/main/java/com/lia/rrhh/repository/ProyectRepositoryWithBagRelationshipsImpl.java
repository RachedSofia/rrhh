package com.lia.rrhh.repository;

import com.lia.rrhh.domain.Proyect;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class ProyectRepositoryWithBagRelationshipsImpl implements ProyectRepositoryWithBagRelationships {

    private static final String ID_PARAMETER = "id";
    private static final String PROYECTS_PARAMETER = "proyects";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<Proyect> fetchBagRelationships(Optional<Proyect> proyect) {
        return proyect.map(this::fetchEmployees);
    }

    @Override
    public Page<Proyect> fetchBagRelationships(Page<Proyect> proyects) {
        return new PageImpl<>(fetchBagRelationships(proyects.getContent()), proyects.getPageable(), proyects.getTotalElements());
    }

    @Override
    public List<Proyect> fetchBagRelationships(List<Proyect> proyects) {
        return Optional.of(proyects).map(this::fetchEmployees).orElse(Collections.emptyList());
    }

    Proyect fetchEmployees(Proyect result) {
        return entityManager
            .createQuery("select proyect from Proyect proyect left join fetch proyect.employees where proyect.id = :id", Proyect.class)
            .setParameter(ID_PARAMETER, result.getId())
            .getSingleResult();
    }

    List<Proyect> fetchEmployees(List<Proyect> proyects) {
        HashMap<Object, Integer> order = new HashMap<>();
        IntStream.range(0, proyects.size()).forEach(index -> order.put(proyects.get(index).getId(), index));
        List<Proyect> result = entityManager
            .createQuery("select proyect from Proyect proyect left join fetch proyect.employees where proyect in :proyects", Proyect.class)
            .setParameter(PROYECTS_PARAMETER, proyects)
            .getResultList();
        Collections.sort(result, (o1, o2) -> Integer.compare(order.get(o1.getId()), order.get(o2.getId())));
        return result;
    }
}
