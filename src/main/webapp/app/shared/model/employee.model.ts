import dayjs from 'dayjs';
import { IRole } from 'app/shared/model/role.model';
import { IArea } from 'app/shared/model/area.model';
import { IProyect } from 'app/shared/model/proyect.model';

export interface IEmployee {
  id?: number;
  firstName?: string | null;
  lastName?: string | null;
  email?: string | null;
  phoneNumber?: string | null;
  cuil?: number | null;
  dni?: number | null;
  birthDate?: dayjs.Dayjs | null;
  hireDate?: dayjs.Dayjs | null;
  salary?: number | null;
  absence?: number | null;
  manager?: IEmployee | null;
  role?: IRole | null;
  area?: IArea | null;
  proyects?: IProyect[] | null;
}

export const defaultValue: Readonly<IEmployee> = {};
