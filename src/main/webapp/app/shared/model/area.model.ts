import { IDepartment } from 'app/shared/model/department.model';

export interface IArea {
  id?: number;
  nameArea?: string | null;
  description?: string | null;
  department?: IDepartment | null;
}

export const defaultValue: Readonly<IArea> = {};
