import dayjs from 'dayjs';
import { IEmployee } from 'app/shared/model/employee.model';
import { IArea } from 'app/shared/model/area.model';

export interface IProyect {
  id?: number;
  nameProyect?: string | null;
  descriptionProyect?: string | null;
  startDate?: dayjs.Dayjs | null;
  endDate?: dayjs.Dayjs | null;
  employees?: IEmployee[] | null;
  areaDestination?: IArea | null;
}

export const defaultValue: Readonly<IProyect> = {};
