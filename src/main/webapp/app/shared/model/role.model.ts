export interface IRole {
  id?: number;
  nameRole?: string | null;
  responsability?: string | null;
}

export const defaultValue: Readonly<IRole> = {};
