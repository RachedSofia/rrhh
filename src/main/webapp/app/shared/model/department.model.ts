export interface IDepartment {
  id?: number;
  nameDepartment?: string;
  description?: string | null;
}

export const defaultValue: Readonly<IDepartment> = {};
