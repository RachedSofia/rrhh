import { IEmployee } from 'app/shared/model/employee.model';

export interface IContract {
  id?: number;
  monthlyGross?: number | null;
  holidays?: number | null;
  employee?: IEmployee | null;
}

export const defaultValue: Readonly<IContract> = {};
