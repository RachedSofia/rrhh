import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Department from './department';
import Area from './area';
import Role from './role';
import Employee from './employee';
import Contract from './contract';
import Proyect from './proyect';
/* jhipster-needle-add-route-import - JHipster will add routes here */

export default () => {
  return (
    <div>
      <ErrorBoundaryRoutes>
        {/* prettier-ignore */}
        <Route path="department/*" element={<Department />} />
        <Route path="area/*" element={<Area />} />
        <Route path="role/*" element={<Role />} />
        <Route path="employee/*" element={<Employee />} />
        <Route path="contract/*" element={<Contract />} />
        <Route path="proyect/*" element={<Proyect />} />
        {/* jhipster-needle-add-route-path - JHipster will add routes here */}
      </ErrorBoundaryRoutes>
    </div>
  );
};
