import React, { useState, useEffect } from 'react';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import { Button, Table } from 'reactstrap';
import { Translate, TextFormat, getSortState } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSort, faSortUp, faSortDown } from '@fortawesome/free-solid-svg-icons';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { ASC, DESC, SORT } from 'app/shared/util/pagination.constants';
import { overrideSortStateWithQueryParams } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntities } from './proyect.reducer';

export const Proyect = () => {
  const dispatch = useAppDispatch();

  const pageLocation = useLocation();
  const navigate = useNavigate();

  const [sortState, setSortState] = useState(overrideSortStateWithQueryParams(getSortState(pageLocation, 'id'), pageLocation.search));

  const proyectList = useAppSelector(state => state.proyect.entities);
  const loading = useAppSelector(state => state.proyect.loading);

  const getAllEntities = () => {
    dispatch(
      getEntities({
        sort: `${sortState.sort},${sortState.order}`,
      }),
    );
  };

  const sortEntities = () => {
    getAllEntities();
    const endURL = `?sort=${sortState.sort},${sortState.order}`;
    if (pageLocation.search !== endURL) {
      navigate(`${pageLocation.pathname}${endURL}`);
    }
  };

  useEffect(() => {
    sortEntities();
  }, [sortState.order, sortState.sort]);

  const sort = p => () => {
    setSortState({
      ...sortState,
      order: sortState.order === ASC ? DESC : ASC,
      sort: p,
    });
  };

  const handleSyncList = () => {
    sortEntities();
  };

  const getSortIconByFieldName = (fieldName: string) => {
    const sortFieldName = sortState.sort;
    const order = sortState.order;
    if (sortFieldName !== fieldName) {
      return faSort;
    } else {
      return order === ASC ? faSortUp : faSortDown;
    }
  };

  return (
    <div>
      <h2 id="proyect-heading" data-cy="ProyectHeading">
        <Translate contentKey="rrhhApp.proyect.home.title">Proyects</Translate>
        <div className="d-flex justify-content-end">
          <Button className="me-2" color="info" onClick={handleSyncList} disabled={loading}>
            <FontAwesomeIcon icon="sync" spin={loading} />{' '}
            <Translate contentKey="rrhhApp.proyect.home.refreshListLabel">Refresh List</Translate>
          </Button>
          <Link to="/proyect/new" className="btn btn-primary jh-create-entity" id="jh-create-entity" data-cy="entityCreateButton">
            <FontAwesomeIcon icon="plus" />
            &nbsp;
            <Translate contentKey="rrhhApp.proyect.home.createLabel">Create new Proyect</Translate>
          </Link>
        </div>
      </h2>
      <div className="table-responsive">
        {proyectList && proyectList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th className="hand" onClick={sort('id')}>
                  <Translate contentKey="rrhhApp.proyect.id">ID</Translate> <FontAwesomeIcon icon={getSortIconByFieldName('id')} />
                </th>
                <th className="hand" onClick={sort('nameProyect')}>
                  <Translate contentKey="rrhhApp.proyect.nameProyect">Name Proyect</Translate>{' '}
                  <FontAwesomeIcon icon={getSortIconByFieldName('nameProyect')} />
                </th>
                <th className="hand" onClick={sort('descriptionProyect')}>
                  <Translate contentKey="rrhhApp.proyect.descriptionProyect">Description Proyect</Translate>{' '}
                  <FontAwesomeIcon icon={getSortIconByFieldName('descriptionProyect')} />
                </th>
                <th className="hand" onClick={sort('startDate')}>
                  <Translate contentKey="rrhhApp.proyect.startDate">Start Date</Translate>{' '}
                  <FontAwesomeIcon icon={getSortIconByFieldName('startDate')} />
                </th>
                <th className="hand" onClick={sort('endDate')}>
                  <Translate contentKey="rrhhApp.proyect.endDate">End Date</Translate>{' '}
                  <FontAwesomeIcon icon={getSortIconByFieldName('endDate')} />
                </th>
                <th>
                  <Translate contentKey="rrhhApp.proyect.employees">Employees</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th>
                  <Translate contentKey="rrhhApp.proyect.areaDestination">Area Destination</Translate> <FontAwesomeIcon icon="sort" />
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {proyectList.map((proyect, i) => (
                <tr key={`entity-${i}`} data-cy="entityTable">
                  <td>
                    <Button tag={Link} to={`/proyect/${proyect.id}`} color="link" size="sm">
                      {proyect.id}
                    </Button>
                  </td>
                  <td>{proyect.nameProyect}</td>
                  <td>{proyect.descriptionProyect}</td>
                  <td>{proyect.startDate ? <TextFormat type="date" value={proyect.startDate} format={APP_DATE_FORMAT} /> : null}</td>
                  <td>{proyect.endDate ? <TextFormat type="date" value={proyect.endDate} format={APP_DATE_FORMAT} /> : null}</td>
                  <td>
                    {proyect.employees
                      ? proyect.employees.map((val, j) => (
                          <span key={j}>
                            <Link to={`/employee/${val.id}`}>{val.id}</Link>
                            {j === proyect.employees.length - 1 ? '' : ', '}
                          </span>
                        ))
                      : null}
                  </td>
                  <td>
                    {proyect.areaDestination ? <Link to={`/area/${proyect.areaDestination.id}`}>{proyect.areaDestination.id}</Link> : ''}
                  </td>
                  <td className="text-end">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`/proyect/${proyect.id}`} color="info" size="sm" data-cy="entityDetailsButton">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`/proyect/${proyect.id}/edit`} color="primary" size="sm" data-cy="entityEditButton">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button
                        onClick={() => (window.location.href = `/proyect/${proyect.id}/delete`)}
                        color="danger"
                        size="sm"
                        data-cy="entityDeleteButton"
                      >
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && (
            <div className="alert alert-warning">
              <Translate contentKey="rrhhApp.proyect.home.notFound">No Proyects found</Translate>
            </div>
          )
        )}
      </div>
    </div>
  );
};

export default Proyect;
