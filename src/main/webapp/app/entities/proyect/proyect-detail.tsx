import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './proyect.reducer';

export const ProyectDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const proyectEntity = useAppSelector(state => state.proyect.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="proyectDetailsHeading">
          <Translate contentKey="rrhhApp.proyect.detail.title">Proyect</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{proyectEntity.id}</dd>
          <dt>
            <span id="nameProyect">
              <Translate contentKey="rrhhApp.proyect.nameProyect">Name Proyect</Translate>
            </span>
          </dt>
          <dd>{proyectEntity.nameProyect}</dd>
          <dt>
            <span id="descriptionProyect">
              <Translate contentKey="rrhhApp.proyect.descriptionProyect">Description Proyect</Translate>
            </span>
          </dt>
          <dd>{proyectEntity.descriptionProyect}</dd>
          <dt>
            <span id="startDate">
              <Translate contentKey="rrhhApp.proyect.startDate">Start Date</Translate>
            </span>
          </dt>
          <dd>{proyectEntity.startDate ? <TextFormat value={proyectEntity.startDate} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <span id="endDate">
              <Translate contentKey="rrhhApp.proyect.endDate">End Date</Translate>
            </span>
          </dt>
          <dd>{proyectEntity.endDate ? <TextFormat value={proyectEntity.endDate} type="date" format={APP_DATE_FORMAT} /> : null}</dd>
          <dt>
            <Translate contentKey="rrhhApp.proyect.employees">Employees</Translate>
          </dt>
          <dd>
            {proyectEntity.employees
              ? proyectEntity.employees.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.id}</a>
                    {proyectEntity.employees && i === proyectEntity.employees.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
          <dt>
            <Translate contentKey="rrhhApp.proyect.areaDestination">Area Destination</Translate>
          </dt>
          <dd>{proyectEntity.areaDestination ? proyectEntity.areaDestination.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/proyect" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/proyect/${proyectEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default ProyectDetail;
