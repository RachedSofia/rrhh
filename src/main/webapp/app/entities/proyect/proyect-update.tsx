import React, { useState, useEffect } from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { Button, Row, Col, FormText } from 'reactstrap';
import { isNumber, Translate, translate, ValidatedField, ValidatedForm } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { IEmployee } from 'app/shared/model/employee.model';
import { getEntities as getEmployees } from 'app/entities/employee/employee.reducer';
import { IArea } from 'app/shared/model/area.model';
import { getEntities as getAreas } from 'app/entities/area/area.reducer';
import { IProyect } from 'app/shared/model/proyect.model';
import { getEntity, updateEntity, createEntity, reset } from './proyect.reducer';

export const ProyectUpdate = () => {
  const dispatch = useAppDispatch();

  const navigate = useNavigate();

  const { id } = useParams<'id'>();
  const isNew = id === undefined;

  const employees = useAppSelector(state => state.employee.entities);
  const areas = useAppSelector(state => state.area.entities);
  const proyectEntity = useAppSelector(state => state.proyect.entity);
  const loading = useAppSelector(state => state.proyect.loading);
  const updating = useAppSelector(state => state.proyect.updating);
  const updateSuccess = useAppSelector(state => state.proyect.updateSuccess);

  const handleClose = () => {
    navigate('/proyect');
  };

  useEffect(() => {
    if (isNew) {
      dispatch(reset());
    } else {
      dispatch(getEntity(id));
    }

    dispatch(getEmployees({}));
    dispatch(getAreas({}));
  }, []);

  useEffect(() => {
    if (updateSuccess) {
      handleClose();
    }
  }, [updateSuccess]);

  // eslint-disable-next-line complexity
  const saveEntity = values => {
    if (values.id !== undefined && typeof values.id !== 'number') {
      values.id = Number(values.id);
    }
    values.startDate = convertDateTimeToServer(values.startDate);
    values.endDate = convertDateTimeToServer(values.endDate);

    const entity = {
      ...proyectEntity,
      ...values,
      employees: mapIdList(values.employees),
      areaDestination: areas.find(it => it.id.toString() === values.areaDestination?.toString()),
    };

    if (isNew) {
      dispatch(createEntity(entity));
    } else {
      dispatch(updateEntity(entity));
    }
  };

  const defaultValues = () =>
    isNew
      ? {
          startDate: displayDefaultDateTime(),
          endDate: displayDefaultDateTime(),
        }
      : {
          ...proyectEntity,
          startDate: convertDateTimeFromServer(proyectEntity.startDate),
          endDate: convertDateTimeFromServer(proyectEntity.endDate),
          employees: proyectEntity?.employees?.map(e => e.id.toString()),
          areaDestination: proyectEntity?.areaDestination?.id,
        };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="rrhhApp.proyect.home.createOrEditLabel" data-cy="ProyectCreateUpdateHeading">
            <Translate contentKey="rrhhApp.proyect.home.createOrEditLabel">Create or edit a Proyect</Translate>
          </h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <ValidatedForm defaultValues={defaultValues()} onSubmit={saveEntity}>
              {!isNew ? (
                <ValidatedField
                  name="id"
                  required
                  readOnly
                  id="proyect-id"
                  label={translate('global.field.id')}
                  validate={{ required: true }}
                />
              ) : null}
              <ValidatedField
                label={translate('rrhhApp.proyect.nameProyect')}
                id="proyect-nameProyect"
                name="nameProyect"
                data-cy="nameProyect"
                type="text"
              />
              <ValidatedField
                label={translate('rrhhApp.proyect.descriptionProyect')}
                id="proyect-descriptionProyect"
                name="descriptionProyect"
                data-cy="descriptionProyect"
                type="text"
              />
              <ValidatedField
                label={translate('rrhhApp.proyect.startDate')}
                id="proyect-startDate"
                name="startDate"
                data-cy="startDate"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('rrhhApp.proyect.endDate')}
                id="proyect-endDate"
                name="endDate"
                data-cy="endDate"
                type="datetime-local"
                placeholder="YYYY-MM-DD HH:mm"
              />
              <ValidatedField
                label={translate('rrhhApp.proyect.employees')}
                id="proyect-employees"
                data-cy="employees"
                type="select"
                multiple
                name="employees"
              >
                <option value="" key="0" />
                {employees
                  ? employees.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <ValidatedField
                id="proyect-areaDestination"
                name="areaDestination"
                data-cy="areaDestination"
                label={translate('rrhhApp.proyect.areaDestination')}
                type="select"
              >
                <option value="" key="0" />
                {areas
                  ? areas.map(otherEntity => (
                      <option value={otherEntity.id} key={otherEntity.id}>
                        {otherEntity.id}
                      </option>
                    ))
                  : null}
              </ValidatedField>
              <Button tag={Link} id="cancel-save" data-cy="entityCreateCancelButton" to="/proyect" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">
                  <Translate contentKey="entity.action.back">Back</Translate>
                </span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" data-cy="entityCreateSaveButton" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp;
                <Translate contentKey="entity.action.save">Save</Translate>
              </Button>
            </ValidatedForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

export default ProyectUpdate;
