import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Proyect from './proyect';
import ProyectDetail from './proyect-detail';
import ProyectUpdate from './proyect-update';
import ProyectDeleteDialog from './proyect-delete-dialog';

const ProyectRoutes = () => (
  <ErrorBoundaryRoutes>
    <Route index element={<Proyect />} />
    <Route path="new" element={<ProyectUpdate />} />
    <Route path=":id">
      <Route index element={<ProyectDetail />} />
      <Route path="edit" element={<ProyectUpdate />} />
      <Route path="delete" element={<ProyectDeleteDialog />} />
    </Route>
  </ErrorBoundaryRoutes>
);

export default ProyectRoutes;
