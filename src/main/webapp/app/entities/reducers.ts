import department from 'app/entities/department/department.reducer';
import area from 'app/entities/area/area.reducer';
import role from 'app/entities/role/role.reducer';
import employee from 'app/entities/employee/employee.reducer';
import contract from 'app/entities/contract/contract.reducer';
import proyect from 'app/entities/proyect/proyect.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

const entitiesReducers = {
  department,
  area,
  role,
  employee,
  contract,
  proyect,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
};

export default entitiesReducers;
