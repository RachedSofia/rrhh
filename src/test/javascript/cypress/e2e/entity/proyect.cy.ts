import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('Proyect e2e test', () => {
  const proyectPageUrl = '/proyect';
  const proyectPageUrlPattern = new RegExp('/proyect(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const proyectSample = {};

  let proyect;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/proyects+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/proyects').as('postEntityRequest');
    cy.intercept('DELETE', '/api/proyects/*').as('deleteEntityRequest');
  });

  afterEach(() => {
    if (proyect) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/proyects/${proyect.id}`,
      }).then(() => {
        proyect = undefined;
      });
    }
  });

  it('Proyects menu should load Proyects page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('proyect');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('Proyect').should('exist');
    cy.url().should('match', proyectPageUrlPattern);
  });

  describe('Proyect page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(proyectPageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create Proyect page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/proyect/new$'));
        cy.getEntityCreateUpdateHeading('Proyect');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', proyectPageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/proyects',
          body: proyectSample,
        }).then(({ body }) => {
          proyect = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/proyects+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [proyect],
            },
          ).as('entitiesRequestInternal');
        });

        cy.visit(proyectPageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details Proyect page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('proyect');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', proyectPageUrlPattern);
      });

      it('edit button click should load edit Proyect page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Proyect');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', proyectPageUrlPattern);
      });

      it('edit button click should load edit Proyect page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('Proyect');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', proyectPageUrlPattern);
      });

      it('last delete button click should delete instance of Proyect', () => {
        cy.intercept('GET', '/api/proyects/*').as('dialogDeleteRequest');
        cy.get(entityDeleteButtonSelector).last().click();
        cy.wait('@dialogDeleteRequest');
        cy.getEntityDeleteDialogHeading('proyect').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', proyectPageUrlPattern);

        proyect = undefined;
      });
    });
  });

  describe('new Proyect page', () => {
    beforeEach(() => {
      cy.visit(`${proyectPageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('Proyect');
    });

    it('should create an instance of Proyect', () => {
      cy.get(`[data-cy="nameProyect"]`).type('wiggly blah barring');
      cy.get(`[data-cy="nameProyect"]`).should('have.value', 'wiggly blah barring');

      cy.get(`[data-cy="descriptionProyect"]`).type('in poorly during');
      cy.get(`[data-cy="descriptionProyect"]`).should('have.value', 'in poorly during');

      cy.get(`[data-cy="startDate"]`).type('2024-05-15T19:30');
      cy.get(`[data-cy="startDate"]`).blur();
      cy.get(`[data-cy="startDate"]`).should('have.value', '2024-05-15T19:30');

      cy.get(`[data-cy="endDate"]`).type('2024-05-15T17:10');
      cy.get(`[data-cy="endDate"]`).blur();
      cy.get(`[data-cy="endDate"]`).should('have.value', '2024-05-15T17:10');

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        proyect = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', proyectPageUrlPattern);
    });
  });
});
