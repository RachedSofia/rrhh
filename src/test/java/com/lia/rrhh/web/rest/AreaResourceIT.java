package com.lia.rrhh.web.rest;

import static com.lia.rrhh.domain.AreaAsserts.*;
import static com.lia.rrhh.web.rest.TestUtil.createUpdateProxyForBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lia.rrhh.IntegrationTest;
import com.lia.rrhh.domain.Area;
import com.lia.rrhh.repository.AreaRepository;
import jakarta.persistence.EntityManager;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link AreaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class AreaResourceIT {

    private static final String DEFAULT_NAME_AREA = "AAAAAAAAAA";
    private static final String UPDATED_NAME_AREA = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/areas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private AreaRepository areaRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAreaMockMvc;

    private Area area;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Area createEntity(EntityManager em) {
        Area area = new Area().nameArea(DEFAULT_NAME_AREA).description(DEFAULT_DESCRIPTION);
        return area;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Area createUpdatedEntity(EntityManager em) {
        Area area = new Area().nameArea(UPDATED_NAME_AREA).description(UPDATED_DESCRIPTION);
        return area;
    }

    @BeforeEach
    public void initTest() {
        area = createEntity(em);
    }

    @Test
    @Transactional
    void createArea() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the Area
        var returnedArea = om.readValue(
            restAreaMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(area)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            Area.class
        );

        // Validate the Area in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        assertAreaUpdatableFieldsEquals(returnedArea, getPersistedArea(returnedArea));
    }

    @Test
    @Transactional
    void createAreaWithExistingId() throws Exception {
        // Create the Area with an existing ID
        area.setId(1L);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restAreaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(area)))
            .andExpect(status().isBadRequest());

        // Validate the Area in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllAreas() throws Exception {
        // Initialize the database
        areaRepository.saveAndFlush(area);

        // Get all the areaList
        restAreaMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(area.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameArea").value(hasItem(DEFAULT_NAME_AREA)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }

    @Test
    @Transactional
    void getArea() throws Exception {
        // Initialize the database
        areaRepository.saveAndFlush(area);

        // Get the area
        restAreaMockMvc
            .perform(get(ENTITY_API_URL_ID, area.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(area.getId().intValue()))
            .andExpect(jsonPath("$.nameArea").value(DEFAULT_NAME_AREA))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }

    @Test
    @Transactional
    void getNonExistingArea() throws Exception {
        // Get the area
        restAreaMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingArea() throws Exception {
        // Initialize the database
        areaRepository.saveAndFlush(area);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the area
        Area updatedArea = areaRepository.findById(area.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedArea are not directly saved in db
        em.detach(updatedArea);
        updatedArea.nameArea(UPDATED_NAME_AREA).description(UPDATED_DESCRIPTION);

        restAreaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedArea.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(updatedArea))
            )
            .andExpect(status().isOk());

        // Validate the Area in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedAreaToMatchAllProperties(updatedArea);
    }

    @Test
    @Transactional
    void putNonExistingArea() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        area.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAreaMockMvc
            .perform(put(ENTITY_API_URL_ID, area.getId()).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(area)))
            .andExpect(status().isBadRequest());

        // Validate the Area in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchArea() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        area.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAreaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(area))
            )
            .andExpect(status().isBadRequest());

        // Validate the Area in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamArea() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        area.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAreaMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(area)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Area in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateAreaWithPatch() throws Exception {
        // Initialize the database
        areaRepository.saveAndFlush(area);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the area using partial update
        Area partialUpdatedArea = new Area();
        partialUpdatedArea.setId(area.getId());

        partialUpdatedArea.description(UPDATED_DESCRIPTION);

        restAreaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedArea.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedArea))
            )
            .andExpect(status().isOk());

        // Validate the Area in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertAreaUpdatableFieldsEquals(createUpdateProxyForBean(partialUpdatedArea, area), getPersistedArea(area));
    }

    @Test
    @Transactional
    void fullUpdateAreaWithPatch() throws Exception {
        // Initialize the database
        areaRepository.saveAndFlush(area);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the area using partial update
        Area partialUpdatedArea = new Area();
        partialUpdatedArea.setId(area.getId());

        partialUpdatedArea.nameArea(UPDATED_NAME_AREA).description(UPDATED_DESCRIPTION);

        restAreaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedArea.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedArea))
            )
            .andExpect(status().isOk());

        // Validate the Area in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertAreaUpdatableFieldsEquals(partialUpdatedArea, getPersistedArea(partialUpdatedArea));
    }

    @Test
    @Transactional
    void patchNonExistingArea() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        area.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAreaMockMvc
            .perform(patch(ENTITY_API_URL_ID, area.getId()).contentType("application/merge-patch+json").content(om.writeValueAsBytes(area)))
            .andExpect(status().isBadRequest());

        // Validate the Area in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchArea() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        area.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAreaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(area))
            )
            .andExpect(status().isBadRequest());

        // Validate the Area in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamArea() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        area.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restAreaMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(area)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Area in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteArea() throws Exception {
        // Initialize the database
        areaRepository.saveAndFlush(area);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the area
        restAreaMockMvc
            .perform(delete(ENTITY_API_URL_ID, area.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return areaRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected Area getPersistedArea(Area area) {
        return areaRepository.findById(area.getId()).orElseThrow();
    }

    protected void assertPersistedAreaToMatchAllProperties(Area expectedArea) {
        assertAreaAllPropertiesEquals(expectedArea, getPersistedArea(expectedArea));
    }

    protected void assertPersistedAreaToMatchUpdatableProperties(Area expectedArea) {
        assertAreaAllUpdatablePropertiesEquals(expectedArea, getPersistedArea(expectedArea));
    }
}
