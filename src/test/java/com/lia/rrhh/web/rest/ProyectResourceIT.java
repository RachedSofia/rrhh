package com.lia.rrhh.web.rest;

import static com.lia.rrhh.domain.ProyectAsserts.*;
import static com.lia.rrhh.web.rest.TestUtil.createUpdateProxyForBean;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.lia.rrhh.IntegrationTest;
import com.lia.rrhh.domain.Proyect;
import com.lia.rrhh.repository.ProyectRepository;
import com.lia.rrhh.service.ProyectService;
import jakarta.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ProyectResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class ProyectResourceIT {

    private static final String DEFAULT_NAME_PROYECT = "AAAAAAAAAA";
    private static final String UPDATED_NAME_PROYECT = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION_PROYECT = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION_PROYECT = "BBBBBBBBBB";

    private static final Instant DEFAULT_START_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String ENTITY_API_URL = "/api/proyects";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ObjectMapper om;

    @Autowired
    private ProyectRepository proyectRepository;

    @Mock
    private ProyectRepository proyectRepositoryMock;

    @Mock
    private ProyectService proyectServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProyectMockMvc;

    private Proyect proyect;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Proyect createEntity(EntityManager em) {
        Proyect proyect = new Proyect()
            .nameProyect(DEFAULT_NAME_PROYECT)
            .descriptionProyect(DEFAULT_DESCRIPTION_PROYECT)
            .startDate(DEFAULT_START_DATE)
            .endDate(DEFAULT_END_DATE);
        return proyect;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Proyect createUpdatedEntity(EntityManager em) {
        Proyect proyect = new Proyect()
            .nameProyect(UPDATED_NAME_PROYECT)
            .descriptionProyect(UPDATED_DESCRIPTION_PROYECT)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE);
        return proyect;
    }

    @BeforeEach
    public void initTest() {
        proyect = createEntity(em);
    }

    @Test
    @Transactional
    void createProyect() throws Exception {
        long databaseSizeBeforeCreate = getRepositoryCount();
        // Create the Proyect
        var returnedProyect = om.readValue(
            restProyectMockMvc
                .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(proyect)))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(),
            Proyect.class
        );

        // Validate the Proyect in the database
        assertIncrementedRepositoryCount(databaseSizeBeforeCreate);
        assertProyectUpdatableFieldsEquals(returnedProyect, getPersistedProyect(returnedProyect));
    }

    @Test
    @Transactional
    void createProyectWithExistingId() throws Exception {
        // Create the Proyect with an existing ID
        proyect.setId(1L);

        long databaseSizeBeforeCreate = getRepositoryCount();

        // An entity with an existing ID cannot be created, so this API call must fail
        restProyectMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(proyect)))
            .andExpect(status().isBadRequest());

        // Validate the Proyect in the database
        assertSameRepositoryCount(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllProyects() throws Exception {
        // Initialize the database
        proyectRepository.saveAndFlush(proyect);

        // Get all the proyectList
        restProyectMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(proyect.getId().intValue())))
            .andExpect(jsonPath("$.[*].nameProyect").value(hasItem(DEFAULT_NAME_PROYECT)))
            .andExpect(jsonPath("$.[*].descriptionProyect").value(hasItem(DEFAULT_DESCRIPTION_PROYECT)))
            .andExpect(jsonPath("$.[*].startDate").value(hasItem(DEFAULT_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllProyectsWithEagerRelationshipsIsEnabled() throws Exception {
        when(proyectServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restProyectMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(proyectServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllProyectsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(proyectServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restProyectMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(proyectRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getProyect() throws Exception {
        // Initialize the database
        proyectRepository.saveAndFlush(proyect);

        // Get the proyect
        restProyectMockMvc
            .perform(get(ENTITY_API_URL_ID, proyect.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(proyect.getId().intValue()))
            .andExpect(jsonPath("$.nameProyect").value(DEFAULT_NAME_PROYECT))
            .andExpect(jsonPath("$.descriptionProyect").value(DEFAULT_DESCRIPTION_PROYECT))
            .andExpect(jsonPath("$.startDate").value(DEFAULT_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()));
    }

    @Test
    @Transactional
    void getNonExistingProyect() throws Exception {
        // Get the proyect
        restProyectMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingProyect() throws Exception {
        // Initialize the database
        proyectRepository.saveAndFlush(proyect);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the proyect
        Proyect updatedProyect = proyectRepository.findById(proyect.getId()).orElseThrow();
        // Disconnect from session so that the updates on updatedProyect are not directly saved in db
        em.detach(updatedProyect);
        updatedProyect
            .nameProyect(UPDATED_NAME_PROYECT)
            .descriptionProyect(UPDATED_DESCRIPTION_PROYECT)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE);

        restProyectMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedProyect.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(updatedProyect))
            )
            .andExpect(status().isOk());

        // Validate the Proyect in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertPersistedProyectToMatchAllProperties(updatedProyect);
    }

    @Test
    @Transactional
    void putNonExistingProyect() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        proyect.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProyectMockMvc
            .perform(put(ENTITY_API_URL_ID, proyect.getId()).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(proyect)))
            .andExpect(status().isBadRequest());

        // Validate the Proyect in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchProyect() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        proyect.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProyectMockMvc
            .perform(
                put(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(om.writeValueAsBytes(proyect))
            )
            .andExpect(status().isBadRequest());

        // Validate the Proyect in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamProyect() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        proyect.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProyectMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(om.writeValueAsBytes(proyect)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Proyect in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateProyectWithPatch() throws Exception {
        // Initialize the database
        proyectRepository.saveAndFlush(proyect);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the proyect using partial update
        Proyect partialUpdatedProyect = new Proyect();
        partialUpdatedProyect.setId(proyect.getId());

        partialUpdatedProyect.endDate(UPDATED_END_DATE);

        restProyectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProyect.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedProyect))
            )
            .andExpect(status().isOk());

        // Validate the Proyect in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertProyectUpdatableFieldsEquals(createUpdateProxyForBean(partialUpdatedProyect, proyect), getPersistedProyect(proyect));
    }

    @Test
    @Transactional
    void fullUpdateProyectWithPatch() throws Exception {
        // Initialize the database
        proyectRepository.saveAndFlush(proyect);

        long databaseSizeBeforeUpdate = getRepositoryCount();

        // Update the proyect using partial update
        Proyect partialUpdatedProyect = new Proyect();
        partialUpdatedProyect.setId(proyect.getId());

        partialUpdatedProyect
            .nameProyect(UPDATED_NAME_PROYECT)
            .descriptionProyect(UPDATED_DESCRIPTION_PROYECT)
            .startDate(UPDATED_START_DATE)
            .endDate(UPDATED_END_DATE);

        restProyectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProyect.getId())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(partialUpdatedProyect))
            )
            .andExpect(status().isOk());

        // Validate the Proyect in the database

        assertSameRepositoryCount(databaseSizeBeforeUpdate);
        assertProyectUpdatableFieldsEquals(partialUpdatedProyect, getPersistedProyect(partialUpdatedProyect));
    }

    @Test
    @Transactional
    void patchNonExistingProyect() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        proyect.setId(longCount.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProyectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, proyect.getId()).contentType("application/merge-patch+json").content(om.writeValueAsBytes(proyect))
            )
            .andExpect(status().isBadRequest());

        // Validate the Proyect in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchProyect() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        proyect.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProyectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, longCount.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(om.writeValueAsBytes(proyect))
            )
            .andExpect(status().isBadRequest());

        // Validate the Proyect in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamProyect() throws Exception {
        long databaseSizeBeforeUpdate = getRepositoryCount();
        proyect.setId(longCount.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProyectMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(om.writeValueAsBytes(proyect)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Proyect in the database
        assertSameRepositoryCount(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteProyect() throws Exception {
        // Initialize the database
        proyectRepository.saveAndFlush(proyect);

        long databaseSizeBeforeDelete = getRepositoryCount();

        // Delete the proyect
        restProyectMockMvc
            .perform(delete(ENTITY_API_URL_ID, proyect.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        assertDecrementedRepositoryCount(databaseSizeBeforeDelete);
    }

    protected long getRepositoryCount() {
        return proyectRepository.count();
    }

    protected void assertIncrementedRepositoryCount(long countBefore) {
        assertThat(countBefore + 1).isEqualTo(getRepositoryCount());
    }

    protected void assertDecrementedRepositoryCount(long countBefore) {
        assertThat(countBefore - 1).isEqualTo(getRepositoryCount());
    }

    protected void assertSameRepositoryCount(long countBefore) {
        assertThat(countBefore).isEqualTo(getRepositoryCount());
    }

    protected Proyect getPersistedProyect(Proyect proyect) {
        return proyectRepository.findById(proyect.getId()).orElseThrow();
    }

    protected void assertPersistedProyectToMatchAllProperties(Proyect expectedProyect) {
        assertProyectAllPropertiesEquals(expectedProyect, getPersistedProyect(expectedProyect));
    }

    protected void assertPersistedProyectToMatchUpdatableProperties(Proyect expectedProyect) {
        assertProyectAllUpdatablePropertiesEquals(expectedProyect, getPersistedProyect(expectedProyect));
    }
}
