package com.lia.rrhh.domain;

import static com.lia.rrhh.domain.AreaTestSamples.*;
import static com.lia.rrhh.domain.EmployeeTestSamples.*;
import static com.lia.rrhh.domain.ProyectTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.lia.rrhh.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class ProyectTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Proyect.class);
        Proyect proyect1 = getProyectSample1();
        Proyect proyect2 = new Proyect();
        assertThat(proyect1).isNotEqualTo(proyect2);

        proyect2.setId(proyect1.getId());
        assertThat(proyect1).isEqualTo(proyect2);

        proyect2 = getProyectSample2();
        assertThat(proyect1).isNotEqualTo(proyect2);
    }

    @Test
    void employeesTest() throws Exception {
        Proyect proyect = getProyectRandomSampleGenerator();
        Employee employeeBack = getEmployeeRandomSampleGenerator();

        proyect.addEmployees(employeeBack);
        assertThat(proyect.getEmployees()).containsOnly(employeeBack);

        proyect.removeEmployees(employeeBack);
        assertThat(proyect.getEmployees()).doesNotContain(employeeBack);

        proyect.employees(new HashSet<>(Set.of(employeeBack)));
        assertThat(proyect.getEmployees()).containsOnly(employeeBack);

        proyect.setEmployees(new HashSet<>());
        assertThat(proyect.getEmployees()).doesNotContain(employeeBack);
    }

    @Test
    void areaDestinationTest() throws Exception {
        Proyect proyect = getProyectRandomSampleGenerator();
        Area areaBack = getAreaRandomSampleGenerator();

        proyect.setAreaDestination(areaBack);
        assertThat(proyect.getAreaDestination()).isEqualTo(areaBack);

        proyect.areaDestination(null);
        assertThat(proyect.getAreaDestination()).isNull();
    }
}
