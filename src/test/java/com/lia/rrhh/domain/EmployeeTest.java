package com.lia.rrhh.domain;

import static com.lia.rrhh.domain.AreaTestSamples.*;
import static com.lia.rrhh.domain.ContractTestSamples.*;
import static com.lia.rrhh.domain.EmployeeTestSamples.*;
import static com.lia.rrhh.domain.EmployeeTestSamples.*;
import static com.lia.rrhh.domain.ProyectTestSamples.*;
import static com.lia.rrhh.domain.RoleTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.lia.rrhh.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class EmployeeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Employee.class);
        Employee employee1 = getEmployeeSample1();
        Employee employee2 = new Employee();
        assertThat(employee1).isNotEqualTo(employee2);

        employee2.setId(employee1.getId());
        assertThat(employee1).isEqualTo(employee2);

        employee2 = getEmployeeSample2();
        assertThat(employee1).isNotEqualTo(employee2);
    }

    @Test
    void contractTest() throws Exception {
        Employee employee = getEmployeeRandomSampleGenerator();
        Contract contractBack = getContractRandomSampleGenerator();

        employee.addContract(contractBack);
        assertThat(employee.getContracts()).containsOnly(contractBack);
        assertThat(contractBack.getEmployee()).isEqualTo(employee);

        employee.removeContract(contractBack);
        assertThat(employee.getContracts()).doesNotContain(contractBack);
        assertThat(contractBack.getEmployee()).isNull();

        employee.contracts(new HashSet<>(Set.of(contractBack)));
        assertThat(employee.getContracts()).containsOnly(contractBack);
        assertThat(contractBack.getEmployee()).isEqualTo(employee);

        employee.setContracts(new HashSet<>());
        assertThat(employee.getContracts()).doesNotContain(contractBack);
        assertThat(contractBack.getEmployee()).isNull();
    }

    @Test
    void managerTest() throws Exception {
        Employee employee = getEmployeeRandomSampleGenerator();
        Employee employeeBack = getEmployeeRandomSampleGenerator();

        employee.setManager(employeeBack);
        assertThat(employee.getManager()).isEqualTo(employeeBack);

        employee.manager(null);
        assertThat(employee.getManager()).isNull();
    }

    @Test
    void roleTest() throws Exception {
        Employee employee = getEmployeeRandomSampleGenerator();
        Role roleBack = getRoleRandomSampleGenerator();

        employee.setRole(roleBack);
        assertThat(employee.getRole()).isEqualTo(roleBack);

        employee.role(null);
        assertThat(employee.getRole()).isNull();
    }

    @Test
    void areaTest() throws Exception {
        Employee employee = getEmployeeRandomSampleGenerator();
        Area areaBack = getAreaRandomSampleGenerator();

        employee.setArea(areaBack);
        assertThat(employee.getArea()).isEqualTo(areaBack);

        employee.area(null);
        assertThat(employee.getArea()).isNull();
    }

    @Test
    void proyectsTest() throws Exception {
        Employee employee = getEmployeeRandomSampleGenerator();
        Proyect proyectBack = getProyectRandomSampleGenerator();

        employee.addProyects(proyectBack);
        assertThat(employee.getProyects()).containsOnly(proyectBack);
        assertThat(proyectBack.getEmployees()).containsOnly(employee);

        employee.removeProyects(proyectBack);
        assertThat(employee.getProyects()).doesNotContain(proyectBack);
        assertThat(proyectBack.getEmployees()).doesNotContain(employee);

        employee.proyects(new HashSet<>(Set.of(proyectBack)));
        assertThat(employee.getProyects()).containsOnly(proyectBack);
        assertThat(proyectBack.getEmployees()).containsOnly(employee);

        employee.setProyects(new HashSet<>());
        assertThat(employee.getProyects()).doesNotContain(proyectBack);
        assertThat(proyectBack.getEmployees()).doesNotContain(employee);
    }
}
