package com.lia.rrhh.domain;

import static com.lia.rrhh.domain.AreaTestSamples.*;
import static com.lia.rrhh.domain.DepartmentTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.lia.rrhh.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class DepartmentTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Department.class);
        Department department1 = getDepartmentSample1();
        Department department2 = new Department();
        assertThat(department1).isNotEqualTo(department2);

        department2.setId(department1.getId());
        assertThat(department1).isEqualTo(department2);

        department2 = getDepartmentSample2();
        assertThat(department1).isNotEqualTo(department2);
    }

    @Test
    void areaTest() throws Exception {
        Department department = getDepartmentRandomSampleGenerator();
        Area areaBack = getAreaRandomSampleGenerator();

        department.addArea(areaBack);
        assertThat(department.getAreas()).containsOnly(areaBack);
        assertThat(areaBack.getDepartment()).isEqualTo(department);

        department.removeArea(areaBack);
        assertThat(department.getAreas()).doesNotContain(areaBack);
        assertThat(areaBack.getDepartment()).isNull();

        department.areas(new HashSet<>(Set.of(areaBack)));
        assertThat(department.getAreas()).containsOnly(areaBack);
        assertThat(areaBack.getDepartment()).isEqualTo(department);

        department.setAreas(new HashSet<>());
        assertThat(department.getAreas()).doesNotContain(areaBack);
        assertThat(areaBack.getDepartment()).isNull();
    }
}
