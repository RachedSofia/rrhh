package com.lia.rrhh.domain;

import static com.lia.rrhh.domain.AreaTestSamples.*;
import static com.lia.rrhh.domain.DepartmentTestSamples.*;
import static com.lia.rrhh.domain.EmployeeTestSamples.*;
import static com.lia.rrhh.domain.ProyectTestSamples.*;
import static org.assertj.core.api.Assertions.assertThat;

import com.lia.rrhh.web.rest.TestUtil;
import java.util.HashSet;
import java.util.Set;
import org.junit.jupiter.api.Test;

class AreaTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Area.class);
        Area area1 = getAreaSample1();
        Area area2 = new Area();
        assertThat(area1).isNotEqualTo(area2);

        area2.setId(area1.getId());
        assertThat(area1).isEqualTo(area2);

        area2 = getAreaSample2();
        assertThat(area1).isNotEqualTo(area2);
    }

    @Test
    void employeeTest() throws Exception {
        Area area = getAreaRandomSampleGenerator();
        Employee employeeBack = getEmployeeRandomSampleGenerator();

        area.addEmployee(employeeBack);
        assertThat(area.getEmployees()).containsOnly(employeeBack);
        assertThat(employeeBack.getArea()).isEqualTo(area);

        area.removeEmployee(employeeBack);
        assertThat(area.getEmployees()).doesNotContain(employeeBack);
        assertThat(employeeBack.getArea()).isNull();

        area.employees(new HashSet<>(Set.of(employeeBack)));
        assertThat(area.getEmployees()).containsOnly(employeeBack);
        assertThat(employeeBack.getArea()).isEqualTo(area);

        area.setEmployees(new HashSet<>());
        assertThat(area.getEmployees()).doesNotContain(employeeBack);
        assertThat(employeeBack.getArea()).isNull();
    }

    @Test
    void proyectTest() throws Exception {
        Area area = getAreaRandomSampleGenerator();
        Proyect proyectBack = getProyectRandomSampleGenerator();

        area.addProyect(proyectBack);
        assertThat(area.getProyects()).containsOnly(proyectBack);
        assertThat(proyectBack.getAreaDestination()).isEqualTo(area);

        area.removeProyect(proyectBack);
        assertThat(area.getProyects()).doesNotContain(proyectBack);
        assertThat(proyectBack.getAreaDestination()).isNull();

        area.proyects(new HashSet<>(Set.of(proyectBack)));
        assertThat(area.getProyects()).containsOnly(proyectBack);
        assertThat(proyectBack.getAreaDestination()).isEqualTo(area);

        area.setProyects(new HashSet<>());
        assertThat(area.getProyects()).doesNotContain(proyectBack);
        assertThat(proyectBack.getAreaDestination()).isNull();
    }

    @Test
    void departmentTest() throws Exception {
        Area area = getAreaRandomSampleGenerator();
        Department departmentBack = getDepartmentRandomSampleGenerator();

        area.setDepartment(departmentBack);
        assertThat(area.getDepartment()).isEqualTo(departmentBack);

        area.department(null);
        assertThat(area.getDepartment()).isNull();
    }
}
