package com.lia.rrhh.domain;

import java.util.Random;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;

public class ProyectTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    public static Proyect getProyectSample1() {
        return new Proyect().id(1L).nameProyect("nameProyect1").descriptionProyect("descriptionProyect1");
    }

    public static Proyect getProyectSample2() {
        return new Proyect().id(2L).nameProyect("nameProyect2").descriptionProyect("descriptionProyect2");
    }

    public static Proyect getProyectRandomSampleGenerator() {
        return new Proyect()
            .id(longCount.incrementAndGet())
            .nameProyect(UUID.randomUUID().toString())
            .descriptionProyect(UUID.randomUUID().toString());
    }
}
