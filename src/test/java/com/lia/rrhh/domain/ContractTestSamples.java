package com.lia.rrhh.domain;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

public class ContractTestSamples {

    private static final Random random = new Random();
    private static final AtomicLong longCount = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));
    private static final AtomicInteger intCount = new AtomicInteger(random.nextInt() + (2 * Short.MAX_VALUE));

    public static Contract getContractSample1() {
        return new Contract().id(1L).monthlyGross(1L).holidays(1);
    }

    public static Contract getContractSample2() {
        return new Contract().id(2L).monthlyGross(2L).holidays(2);
    }

    public static Contract getContractRandomSampleGenerator() {
        return new Contract()
            .id(longCount.incrementAndGet())
            .monthlyGross(longCount.incrementAndGet())
            .holidays(intCount.incrementAndGet());
    }
}
